package biz.r2s.util.crud

import biz.r2s.util.GrailsUtil
import grails.transaction.Transactional
import org.codehaus.groovy.grails.commons.GrailsDomainClass
import org.codehaus.groovy.grails.commons.GrailsDomainClassProperty
import org.springframework.util.Assert

class HasManyRestfulController<T> extends RestfulController<T> {
    String propertyHasMany

    def HasManyRestfulController(Class<T> resource, Class<CrudService> serviceClass) {
        super(resource, serviceClass)
    }

    def HasManyRestfulController(Class<T> resource) {
        super(resource)
    }

    def HasManyRestfulController(Class<T> resource, String propertyHasMany, Class<CrudService> serviceClass) {
        super(resource, serviceClass)
        this.propertyHasMany = propertyHasMany
    }

    def HasManyRestfulController(Class<T> resource, String propertyHasMany) {
        super(resource)
        this.propertyHasMany = propertyHasMany
    }

    @Override
    protected Class getResourceDomain() {
        if (isHasMany()) {
            return this.getResourseHasMany()
        } else {
            return this.getResource()
        }
    }

    private Class getResourseHasMany() {
        Class resourceOrg = this.getResource()
        GrailsDomainClass domainClass = GrailsUtil.getDomainClass(resourceOrg)
        String phm = getPropertyHasMany()
        GrailsDomainClassProperty classProperty = domainClass.getPropertyByName(phm)
        Assert.notNull(classProperty, "o campos hasMany $phm não existe")
        return classProperty.referencedDomainClass.clazz
    }

    private Long getIdFather() {
        return utilCrudService.getIdFatherByUrl(request.forwardURI)
    }

    @Override
    protected List<T> listAllResources(Map parametros) {
        def p = [max:parametros.max,offset:parametros.offset]
        if (isHasMany()) {

            Class resourceHasMany = this.getResourseHasMany()
            Class resourceFather = this.getResource()
            String phm = getPropertyHasMany()
            String idFather = this.getIdFather()
            return resourceHasMany.executeQuery("""
                select r
                from $resourceHasMany.simpleName as r
                where r.id in (
                               select c.id
                               from $resourceFather.simpleName as f
                               inner join f.$phm as c where f.id = $idFather
                )
            """, p )
        } else {
            super.listAllResources(p)
        }
    }

    @Override
    protected Integer countResources() {
        if (isHasMany()) {
            Class resourceHasMany = this.getResourseHasMany()
            Class resourceFather = this.getResource()
            String phm = getPropertyHasMany()
            String idFather = this.getIdFather()
            return resourceHasMany.executeQuery("""
                select count(*)
                from $resourceHasMany.simpleName
                where id in (
                               select c.id
                               from $resourceFather.simpleName as f
                               inner join f.$phm as c where f.id = $idFather
                )
            """).first()
        } else {
            super.countResources()
        }
    }

    protected T queryForResource(Serializable id) {
        if (isHasMany()) {
            Class resourceHasMany = this.getResourseHasMany()
            Class resourceFather = this.getResource()
            String phm = getPropertyHasMany()
            String idFather = this.getIdFather()
            def list = resourceHasMany.executeQuery("""
                select r
                from $resourceHasMany.simpleName as r
                where id in (
                               select c.id
                               from $resourceFather.simpleName as f
                               inner join f.$phm as c where f.id = $idFather
                ) and id = $id
            """)

            return list ? list.first() : null
        } else {
            super.queryForResource(id)
        }
    }

    def save() {
        if (isHasMany()) {
            this.actionDefaultSave(
                    { return createResource(params) },
                    { instance ->

                        def objeto = service.salvar(instance)
                        Class resourceHasMany = this.getResourseHasMany()
                        Class resourceFather = this.getResource()
                        String phm = getPropertyHasMany()
                        String idFather = this.getIdFather()
                        def father = resourceFather.get(idFather)
                        if (father) {
                            father."addTo${phm.capitalize()}"(objeto)
                            father.save(validate: false, failOnError: true, deepValidate: false, flush: true)
                        }
                        return objeto
                    }
            )
        } else {
            super.save()
        }
    }

    @Transactional
    def delete() {
        if (isHasMany()) {
            this.actionDefaultDelete(
                    { return obterInstancia(params) },
                    { instance ->
                        Class resourceHasMany = this.getResourseHasMany()
                        Class resourceFather = this.getResource()
                        String phm = getPropertyHasMany()
                        String idFather = this.getIdFather()
                        def father = resourceFather.get(idFather)

                        if (father) {
                            father."removeFrom${phm.capitalize()}"(instance)
                            instance.delete()
                        }
                    }
            )
        } else {
            super.delete()
        }

    }

    boolean isHasMany() {
        if (this.getPropertyHasMany()) {
            return true
        }
        return false
    }
}
