package biz.r2s.util.crud

//import grails.plugin.springsecurity.annotation.Secured

import java.lang.reflect.Method

class SecRestfulController<T> extends SimplesRestfullController<T>{

    SegurancaCrudService segurancaCrudService

    def SecRestfulController(Class<T> resource, Class<CrudService> serviceClass) {
        super(resource, serviceClass)
    }

    def SecRestfulController(Class<T> resource) {
        super(resource)
    }

    Map roles = [:]

    def beforeInterceptor = [action: this.&auth, except: 'notAcess']

    public def auth(){
        if(!hasPermission()){
            notAcess()
        }else{
            executePermission(actionName)
        }
    }

    protected def executePermission(String actionName){

    }

    public def notAcess(){
        render formatError.format(response, new Exception("Usuário não tem acesso a ação: $actionName"), formatError.PROIBIDO)
    }

    def hasPermission(){
        def rolesController

        def rolesCustom = roles.get(actionName)
        Class controllerClass = this.getClass()
        Method methodAction = controllerClass.getMethod(actionName)
        if(rolesCustom){
            rolesController = rolesCustom
        }/*else if(controllerClass.isAnnotationPresent(Secured)){
            Secured secured = controllerClass.getAnnotation(Secured)
            rolesController = secured.value()
        }else if(methodAction .getAnnotation(Secured)){
            Secured secured = controllerClass.getAnnotation(Secured)
            rolesController = secured.value()
        }*/else {
            rolesController = segurancaCrudService.getRoleDefault(this.getResourceDomain(), actionName)
        }
        return segurancaCrudService.hasPermissionSprintSecurity(rolesController)
    }

}
