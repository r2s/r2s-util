package biz.r2s.util.crud

import biz.r2s.util.ErrorFormat
import biz.r2s.util.renderer.ApiJsonRenderer
import biz.r2s.util.GrailsUtil
import grails.rest.RestfulController
import grails.transaction.Transactional
import grails.util.Holders
import org.codehaus.groovy.grails.commons.GrailsServiceClass
import org.springframework.http.HttpStatus

class SimplesRestfullController<T>  extends RestfulController<T> {

    CrudService<T> service
    UtilCrudService utilCrudService

    static responseFormats = ['json', 'xml']

    static int OFFSET_DEFAULT = 0
    static int MAX_DEFAULT = 10
    static String ORDER_DEFAULT = "desc"
    static String SORT_DEFAULT = "id"

    static ErrorFormat formatError

    CrudService<T> getService() {
        def serv = this.@service
        if(!serv){
            Class resourceServ = this.getResourceDomain()
            def custom = this.getServiceCustom(resourceServ)
            if(custom){
                serv = custom
            }else{
                serv = this.getServiceDefault(resourceServ)
            }
        }
        return serv
    }

    protected Class getResourceDomain(){
        return this.getResource()
    }

    private Long getIdDomain(){
        return utilCrudService.getIdByUrl(request.forwardURI)
    }

    private CrudService<T> getServiceDefault(Class resourceClass){
        def serviceDefault = GrailsUtil.getGrailsApplication().mainContext.getBean(DefaultCrudService)
        serviceDefault.resource = resourceClass
        return serviceDefault
    }

    private CrudService<T> getServiceCustom(Class resourceClass){
        def serviceCustom = null

        for(GrailsServiceClass grailsServiceClass :GrailsUtil.getGrailsApplication().serviceClasses){
            if(grailsServiceClass.clazz.isAssignableFrom(CrudService)&&!grailsServiceClass.clazz in [CrudService, DefaultCrudService]){
                CrudService instanciaService = GrailsUtil.getGrailsApplication().mainContext.getBean(grailsServiceClass.clazz)
                if(instanciaService.resource == resourceClass){
                    serviceCustom = instanciaService
                    break;
                }
            }
        }
        return serviceCustom
    }

    def SimplesRestfullController(Class<T> resource, Class<CrudService> serviceClass) {
        super(resource, serviceClass)
        this.service = Holders.grailsApplication.mainContext.getBean(serviceClass)
        formatError = new ErrorFormat()
    }

    def SimplesRestfullController(Class<T> resource) {
        super(resource)
        formatError = new ErrorFormat()
    }

    def index() {
        try {
            def clusureBuscaDados, closureCount, closureFilterCount
            prepararParametrosConsulta()

            clusureBuscaDados = {
                return listAllResources(params)
            }
            closureCount = {
                return countResources()
            }
            this.actionDefaultListPagination(clusureBuscaDados, closureCount, closureFilterCount)

        } catch (Exception e) {
            render formatError.format(response, e)
        }
    }

    @Override
    protected List<T> listAllResources(Map parametros) {
        return this.getResource().list(parametros)
    }

    @Override
    protected Integer countResources() {
        return this.getResource().count()
    }

    @Override
    protected T createResource() {
        this.getResourceDomain().newInstance()
    }

    @Override
    protected T createResource(Map params) {
        T instance = this.getResourceDomain().newInstance()
        bindData instance, getObjectToBind()
        instance
    }

    protected T queryForResource(Serializable id) {
        return this.getResourceDomain().get(id)
    }

    def show() {
        actionDefaultShow({ return obterInstancia(params) })
    }

    def save() {
        this.actionDefaultSave(
                { return createResource(params) },
                { instance ->
                    return this.getService().salvar(instance)
                }
        )
    }

    protected def getObjectToBind() {
        request.JSON
    }

    @Transactional
    def update() {
        this.actionDefaultUpdate(
                { return obterInstancia(params) },
                { return getObjectToBind() },
                { instance -> return this.getService().editar(instance) }
        )
    }



    @Transactional
    def delete() {
        this.actionDefaultDelete(
                { return obterInstancia(params) },
                { instance ->
                    instance.delete()
                }
        )
    }

    protected def actionDefaultUpdate(def clusureObterInstancia, def closureBind, def closureEditar) {
        if (handleReadOnly()) {
            return
        }

        def instance = clusureObterInstancia()
        this.validarInstancia(instance)

        if (!this.validarVersion(instance)) {
            return
        }
        instance.properties = closureBind()

        try {
            instance = closureEditar(instance)
        }
        catch (Exception e) {
            render formatError.format(response, e, null)
            return
        }
        formatRequestSucesso(instance, response, request, params, org.springframework.http.HttpStatus.OK)
    }



    protected def actionDefaultSave(def clusureCreateResource, def closureSalvar) {
        if (handleReadOnly()) {
            return
        }
        def instance = clusureCreateResource()

        try {
            instance = closureSalvar(instance)
        }
        catch (Exception e) {
            render formatError.format(response, e, null)
            return
        }
        formatRequestSucesso(instance, response, request, params, org.springframework.http.HttpStatus.CREATED)
    }

    protected def actionDefaultDelete(def closureObterInstancia, def closureDelete) {
        if (handleReadOnly()) {
            return
        }

        def instance = closureObterInstancia()

        try {
            closureDelete(instance)
        }
        catch (Exception e) {
            render formatError.format(response, e, null)
        }
        formatRequestSucesso(instance, response, request, params, org.springframework.http.HttpStatus.OK)
    }


    protected def actionDefaultShow(def closureShow) {
        def objeto = closureShow()
        this.validarInstancia(objeto)
        respond objeto, params
    }

    protected def obterInstancia(parametros) {
        String id = getIdDomain()
        def objeto = null
        if (id) {
            objeto = queryForResource(id)
        }
        return objeto
    }

    protected def formatRequestSucesso(objeto, response, request, parametros, HttpStatus code) {
        response.setStatus(code.value())
        respond objeto, parametros
    }

    protected boolean validarVersion(instance) {
        def currentVersion = instance.hasProperty("version") ? instance.version : 0
        def originalVersion = request.JSON.version ?: 0
        if (originalVersion == currentVersion) {
            return true
        } else {
            render formatError.format(response, new Exception("Outro usuário atualizou esse objeto enquanto você estava editando."), formatError.CONFLITO)
            return false
        }
    }

    protected def actionDefaultListPagination(clusureBuscaDados, closureCount, closureFilterCount = null) {
        try {
            prepararParametrosConsulta()
            def lista = clusureBuscaDados()
            Integer count = closureCount()
            Integer filterCount = closureFilterCount ? closureFilterCount() : count
            preparaParametrosGrid(filterCount, count)

            this.actionDefaultList(lista, params)
        } catch (Exception e) {
            render formatError.format(response, e)
        }
    }

    protected def actionDefaultList(lista, parametros) {
        try {
            if (lista) {
                respond lista, parametros
            } else {
                //TODO: http://stackoverflow.com/questions/21592731/grails-xml-marshalling-change-default-list-root-element-name
                def grid = new ApiJsonRenderer().trataCollection(lista, parametros)
                respond grid
            }

        } catch (Exception e) {
            render formatError.format(response, e)
        }
    }

    protected void preparaParametrosGrid(totalFiltrado, totalRegistros) {
        params.page = calcularPage(params)
        params.recordsFiltered = totalFiltrado
        params.recordsTotal = totalRegistros
        params.recordsPerPage = params.long('max')
        params.draw = params.long('draw')
    }

    protected void prepararParametrosConsulta() {
        params.offset = obterOffset(params)
        params.max = obterMax(params)
        params.order = obterOrder(params)
        params.sort = obterSort(params)
    }

    protected def obterOffset(parametros) {
        def offset = parametros.offset ?: OFFSET_DEFAULT
        if (params.start) {
            offset = params.start
        }
        return offset
    }

    protected def obterMax(parametros) {
        def max = parametros.max ?: MAX_DEFAULT
        if (params.length) {
            max = params.length
        }
        return max
    }

    protected def obterOrder(parametros) {
        return parametros.order ?: ORDER_DEFAULT
    }

    protected def obterSort(parametros) {
        return parametros.sort ?: SORT_DEFAULT
    }


    protected def calcularPage(parametros) {
        if (parametros?.offset && parametros?.max) {
            return Math.ceil(parametros?.double('offset') / parametros?.double('max'))?.toInteger()
        }
        return 1
    }

    protected def validarInstancia(instance) {
        if (!instance) {
            render formatError.format(response, new Exception("${obterAliasInstancia()} não encontrado"), formatError.OBJETO_NAO_ENCONTRADO)
        }
    }

    private String obterAliasInstancia() {
        String alias
        if (getService() != null) {
            alias = getService()?.aliasInstancia
        } else {
            alias = this.getResourceDomain().simpleName
        }
        return alias
    }
}
