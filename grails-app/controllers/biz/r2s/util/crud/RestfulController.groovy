package biz.r2s.util.crud

class RestfulController<T> extends SecRestfulController<T> {
    def RestfulController(Class<T> resource, Class<CrudService> serviceClass) {
        super(resource, serviceClass)
    }

    def RestfulController(Class<T> resource) {
        super(resource)
    }

    def index() {
        try {
            def listaSearch = ['q']
            def query = ""
            listaSearch.each {
                if (params."$it") {
                    query = params."$it"
                }
            }
            def clusureBuscaDados, closureCount, closureFilterCount
            prepararParametrosConsulta()

            if (query) {
                def result = findSearch(query, params)
                clusureBuscaDados = {
                    return this.getResourceDomain().getAll(result.searchResults.id)
                }

                closureFilterCount = {
                    return result.total
                }

                closureCount = {
                    return countResources()
                }

            } else {
                clusureBuscaDados = {
                    return listAllResources(params)
                }
                closureCount = {
                    return countResources()
                }
            }
            this.actionDefaultListPagination(clusureBuscaDados, closureCount, closureFilterCount)

        } catch (Exception e) {
            render formatError.format(response, e)
        }
    }


    def descendentes() {
        this.actionListAssociation({ return obterInstancia(params) },
                { instance -> this.getService().listarDescendentes(instance) }
        )
    }

    def ascendentes() {
        this.actionListAssociation({ return obterInstancia(params) },
                { instance -> return this.getService().listarAscendentes(instance) }
        )
    }

    def children() {
        this.actionListAssociation({
            def instance = null
            def id = getIdDomain()

            if (!id) {
                instance = this.getResourceDomain().newInstance()
            } else {
                instance = obterInstancia(params)
            }
            return instance
        },
                { instance ->
                    if (instance.id)
                        return this.getService().listarChildren(instance)
                    else
                        return this.getService().listarRaizes()
                }
        )
    }

    protected def actionListAssociation(closureObterInstancia, clusureListAssociation) {
        try {
            def instance = closureObterInstancia()
            this.validarInstancia(instance)
            def lista = clusureListAssociation(instance)
            this.actionDefaultList(lista, params)
        } catch (Exception e) {
            render formatError.format(response, e)
        }
    }

    protected def findSearch(String query, parametros) {
        if (isSearchable()) {
            return this.getResource().search(formatQuerySearch(query, parametros), [from: parametros.offset ?: 0, size: parametros.max ?: 10])
        } else {
            response.status = 405
            throw new Exception("Índice não disponível. Contate suporte.")
        }
    }

    protected String formatQuerySearch(String query, parametros) {
        return "${query}*"
    }

    private boolean isSearchable() {
        return this.getResource().metaClass.respondsTo(resource.newInstance(), "search")
    }
}
