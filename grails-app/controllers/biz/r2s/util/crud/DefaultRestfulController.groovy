package biz.r2s.util.crud

//import grails.plugin.springsecurity.annotation.Secured

//@Secured(["IS_AUTHENTICATED_FULLY"])
class DefaultRestfulController extends HasManyRestfulController<Object> {

    DefaultRestfulController() {
        super(Object)
        this.resourceName = "defaultRestful"
    }

    DefaultRestfulController(String resourceName) {
        super(Object)
        this.resourceName = resourceName
    }

    static scope = "prototype"

    @Override
    Class getResource() {
        this.getResourceParams()
    }

    Class getResourceParams(){
        params.resource
    }

    @Override
    String getPropertyHasMany() {
        this.getPropertyHasManyParams()
    }

    String getPropertyHasManyParams(){
        params.propertyHasMany
    }

}
