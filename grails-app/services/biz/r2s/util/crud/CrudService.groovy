package biz.r2s.util.crud

import biz.r2s.util.Hierarquizado
import grails.transaction.Transactional
import grails.validation.ValidationException
import org.springframework.util.Assert

@Transactional
class CrudService<T>{

    Class<T> resource

    T salvar(T instance){
        if(instance.validate()){
            return instance.save(failOnError: true)
        }else{
            throw new ValidationException("Erro ao salvar "+this.getAliasInstancia(), instance.errors)
        }
    }

    T editar(T instance){
        Assert.notNull(instance.get(instance.id), this.getAliasInstancia()+" não encontrado para o id: ${instance.id}")
        if(instance.validate()){
            return instance.save(failOnError: true)
        }else{
            throw new ValidationException("Erro ao editar "+this.getAliasInstancia(), instance.errors)
        }
    }

    T delete(Long id){
        T instance = T.get(id)
        Assert.notNull(instance, this.getAliasInstancia()+" não encontrado para o id: ${id}")
        return this.delete(instance)
    }

    boolean delete(T instance){
        Assert.notNull(instance, this.getAliasInstancia()+" não pode ser null")
        return instance.delete()
    }

    String getAliasInstancia(){
        return T.class.getSimpleName()
    }

    List<T> listarDescendentes(T root, Boolean includeRoot = false) {
        if(!root){
            return null
        }

        if(!(root instanceof Hierarquizado)){
            throw new RuntimeException("O ${this.aliasInstancia} não suporta essa funcianalidade")
        }

        Hierarquizado<T> objeto = root
        def lista = []
        if (includeRoot) {
            lista += objeto
        }

        objeto?.childrens?.each {
            lista.addAll(listarDescendentes(it, true))
        }
        return lista.unique()
    }

    List<T> listarAscendentes(T root, Boolean includeRoot = false) {
        if(!root){
            return null
        }

        if(!(root instanceof Hierarquizado)){
            throw new RuntimeException("O ${this.aliasInstancia} não suporta essa funcianalidade")
        }

        Hierarquizado<T> objeto = root
        def listaAscendentes = includeRoot ? [objeto] : []
        while (objeto.parent) {
            listaAscendentes << objeto.parent
            objeto = objeto.parent
        }
        return listaAscendentes
    }

    List<T> listarRaizes() {
        def listaPropriedadeParents = ["parent", "pai"]

        T instancia = resource.newInstance()

        if(!(instancia instanceof Hierarquizado)){
            throw new RuntimeException("O ${this.aliasInstancia} não suporta essa funcianalidade")
        }

        Hierarquizado<T> objeto = instancia

        def listaRaizes = []

        listaPropriedadeParents.each { propriedade ->
            if(objeto?.domainClass?.persistentProperties.findAll{ it.name == "$propriedade"}?.size() > 0){
                listaRaizes.addAll(objeto.findAll( "from ${objeto.class.getSimpleName()} as c where c.$propriedade is null"))
            }
        }
        return listaRaizes
    }


    List<T> listarChildren(T root, Boolean includeRoot = false) {
        if(!root){
            return null
        }

        if(!(root instanceof Hierarquizado)){
            throw new RuntimeException("O ${this.aliasInstancia} não suporta essa funcianalidade")
        }

        Hierarquizado<T> objeto = root

        return objeto.childrens?.collect({it})
    }


}