package biz.r2s.util.crud

import biz.r2s.util.GrailsUtil
import grails.transaction.Transactional

@Transactional
class SegurancaCrudService {
    def INCLUDE_PERMISSION = ["IS_AUTHENTICATED_ANONYMOUSLY", "IS_AUTHENTICATED_FULLY"]

    private Map<String, String> actionMethods = [
            "save":"CREATE",
            "update":"EDIT",
            "show":"VIEW",
            "delete":"DELETE",
            "index":"LIST",
            "ascendentes":"LIST",
            "descendentes":"LIST",
            "children":"LIST"
    ]

    def getRoleDefault(Class domain, String action) {
        String actionName = this.formatNameAction(action)
        this.getRoleDefaultByName(domain, actionName)
    }

    def formatNameAction(String action) {
        def tas = this.actionMethods.get(action)
        if (tas) {
            return tas
        } else {
            return action
        }
    }

    def getRoleDefaultByName(Class domain, String actionName) {
        String moduloName = this.getModulo(domain)
        String domainName = this.formatDomainName(domain)

        return "ROLE_${moduloName}_${domainName}_${actionName}".toUpperCase()
    }

    def getModulo(Class domain) {
        GrailsUtil.getNameModulo(domain)
    }

    def formatDomainName(Class domain) {
        return domain.simpleName
    }

    private String formatRoles(def roles){
        if(roles instanceof String){
            return roles
        }
        return roles?.flatten()?.join(",")
    }

    def hasPermissionSprintSecurity(def roles){
        hasPermissionSprintSecurity(formatRoles(roles))
    }

    def hasPermissionSprintSecurity(String roles){
        def retorno = false
        INCLUDE_PERMISSION.each {
            if(it in roles.split(","))  {
                retorno = true
            }
        }
        return retorno?:true
    }

}
