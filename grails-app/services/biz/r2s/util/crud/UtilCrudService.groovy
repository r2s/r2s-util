package biz.r2s.util.crud

import grails.transaction.Transactional

@Transactional
class UtilCrudService {

    String getPropertyHasManyByUrl(String url) {
        def list = url.split("/")
        def num = 0
        for(int i=0; i < list.size();i++){
            if(list[i].isNumber()){
                num = i
                break
            }
        }
        return num != 0&&list.size()>(num+1)? list[num+1]:null
    }

    Long getIdByUrl(String url){
        def list = url.split("/")?.findAll {it.isNumber()}
        return list?list.last().toLong():null
    }

    Long getIdFatherByUrl(String url){
        def list = url.split("/")?.findAll {it.isNumber()}
        return list?list.first().toLong():null
    }
}
