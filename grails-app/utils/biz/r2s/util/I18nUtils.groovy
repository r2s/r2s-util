package biz.r2s.util

import org.springframework.context.NoSuchMessageException

/**
 * Created by raphael on 28/07/15.
 */
class I18nUtils {
    static String getMessage(String key, String name=null){
        if(name){
            key = key.replace(":name",name.toLowerCase())
        }
        try {
            getMessageResource().getMessage(key, null, "", null)
        }
        catch (NoSuchMessageException e) {
            return null
        }
    }
    static String getMessage(List<String> keys, String name = null){
        String messageReturn = null
        for(String key:keys){
            String keyO = key
            if(name){
                keyO = key.replace(":name",name.toLowerCase())
            }

            String message = I18nUtils.getMessage(keyO)
            if(message){
                messageReturn =  message
                break;
            }
        }
        return messageReturn
    }

    static getMessageResource() {
        return GrailsUtil.getGrailsApplication().mainContext.getBean("messageSource")
    }
}
