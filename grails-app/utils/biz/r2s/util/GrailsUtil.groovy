package biz.r2s.util

import grails.util.Holders
import org.codehaus.groovy.grails.commons.GrailsApplication
import org.codehaus.groovy.grails.commons.GrailsDomainClass
import org.codehaus.groovy.grails.commons.GrailsDomainClassProperty

/**
 * Created by raphael on 30/07/15.
 */
class GrailsUtil {

    static String PREFIX_PLUGIN = "sagui"

    public static GrailsApplication getGrailsApplication() {
        Holders.grailsApplication
    }

    public static GrailsDomainClass getDomainClass(Class clazz) {
        def ga = this.getGrailsApplication()
        GrailsDomainClass domainClass = null
        if (ga.isDomainClass(clazz)) {
            domainClass = ga.getDomainClass(clazz.name)
        }
        return domainClass
    }

    public static List<GrailsDomainClassProperty> getPersistentProperties(Class clazz) {
        def domain = this.getDomainClass(clazz)

        if (domain) {
            return this.getPersistentProperties(domain)
        }
        return null
    }

    public static <T> T getBean(Class<T> clazz){
        return this.getGrailsApplication().mainContext.getBean(clazz)
    }

    public static List<GrailsDomainClassProperty> getPersistentProperties(GrailsDomainClass gdc) {
        def props = []
        if (gdc) {
            gdc.getProperties().each {
                if (it.isPersistent()) {
                    props.add(it)
                }
            }
        }
        return props
    }

    public static String getNameModulo(Class domain) {
        String name = Holders.pluginManager.getPluginForClass(domain).name
        name.toLowerCase().replaceFirst(PREFIX_PLUGIN, "")
    }

}
