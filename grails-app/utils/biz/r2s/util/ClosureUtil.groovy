package biz.r2s.util



/**
 * Created by raphael on 04/08/15.
 */
class ClosureUtil {


    static Map toMapConfig(def closure, Class aClass, List nameIncludes){
        def prop = [:]
        closure.metaClass.invokeMethod = { String name, Object args ->
            if ((aClass && GrailsUtil.getDomainClass(aClass).getPersistentProperty(name)!=null) || name in nameIncludes) {
                prop.put(name, args[0])
            } else {
                def metaMethod = closure.metaClass.getMetaMethod(name, args)
                metaMethod.invoke(closure, args)
            }
        }

        def runClosure = {
            closure.call()
        }

        runClosure()
        return prop
    }

}
