package biz.r2s.util

import grails.converters.JSON
import grails.util.Environment
import grails.util.Holders
import grails.validation.ValidationException
import org.apache.commons.logging.LogFactory
import org.springframework.context.MessageSourceResolvable
import org.springframework.context.NoSuchMessageException
import org.springframework.http.HttpStatus

/**
 * Created by raphael on 08/06/15.
 */
public class ErrorFormat {
    static HttpStatus REQUISICAO_MAL_FORMADA = HttpStatus.BAD_REQUEST
    static HttpStatus NAO_AUTORIZADO = HttpStatus.UNAUTHORIZED
    static HttpStatus PROIBIDO = HttpStatus.FORBIDDEN
    static HttpStatus NAO_ENCONTRADO = HttpStatus.NOT_FOUND
    static HttpStatus OBJETO_NAO_ENCONTRADO = HttpStatus.GONE
    static HttpStatus METODO_HTTP_NAO_PERMITIDO = HttpStatus.METHOD_NOT_ALLOWED
    static HttpStatus FALHA_NA_VALIDACAO = HttpStatus.PRECONDITION_FAILED
    static HttpStatus ERRO_INTERNO = HttpStatus.INTERNAL_SERVER_ERROR
    static HttpStatus CONFLITO = HttpStatus.CONFLICT

    private static final log = LogFactory.getLog(this)

    def getMessageResource() {
        return Holders.grailsApplication.mainContext.getBean("messageSource")
    }

    @Deprecated
    def formatter(response, Exception e, HttpStatus code_error = null) {
        this.format(response, e, code_error)
    }

    def format(response, Exception e, HttpStatus code_error = null) {
        if (code_error) {
            //tratar cada code_error
            return formatExceptionCode(response, e, code_error)
        } else {
            if (e instanceof ValidationException) {
                return formatValidationException(response, e)
            } else {
                return formatNormalException(response, e)
            }
        }
    }

    private def formatExceptionCode(response, Exception e, HttpStatus code_error) {
        code_error = code_error ?: ERRO_INTERNO
        response.setStatus(code_error.value())
        return formatarMensagemError(e)
    }

    private def formatNormalException(response, Exception e) {
        response.setStatus(FALHA_NA_VALIDACAO.value())
        return formatarMensagemError(e)
    }

    private def formatValidationException(response, ValidationException e) {
        HashMap<String, String> listaErros = retrieveErrorList(e)
        response.setStatus(FALHA_NA_VALIDACAO.value())
        listaErros as JSON
    }

    private HashMap<String, String> retrieveErrorList(ValidationException excecao) {
        def listaErros = new HashMap<String, String>()
        def errors = excecao.errors.fieldErrors
        errors.each { error ->
            def campo = error.field.toString()
            def mensagemErro = encontrarMensagemPorCodigo(error)
            if (listaErros.containsKey(campo)) {
                listaErros["${campo}"] << mensagemErro
            } else {
                listaErros.put(campo, [mensagemErro])
            }
        }

        excecao.errors.globalErrors.each { error ->
            def campo = "error_message"
            def mensagemErro = encontrarMensagemPorCodigo(error)
            if (listaErros.containsKey(campo)) {
                listaErros["${campo}"] = listaErros["${campo}"].concat(mensagemErro)
            } else {
                listaErros.put(campo, mensagemErro)
            }
        }
        listaErros
    }

    private def formatarMensagemError(Exception e) {
        String mensagem = ""
        if (e?.message) {
            mensagem = encontrarMensagemPorCodigo(e.message)
        } else {
            log.error("Erro não previsto", e)
        }
        def retorno = ["error_message": mensagem]
        if (Environment.current == Environment.DEVELOPMENT) {
            retorno.stackTrace = e.stackTrace
        }
        return retorno as JSON
    }

    private String encontrarMensagemPorCodigo(MessageSourceResolvable error) {
        String mensagem = ""

        if (error) {
            try {
                mensagem = getMessageResource().getMessage(error, null)
            }
            catch (NoSuchMessageException e) {
                mensagem = error.getDefaultMessage()
            }
        }
        return mensagem
    }

    private String encontrarMensagemPorCodigo(String codigo) {
        try {
            getMessageResource().getMessage(codigo, null, codigo, null)
        }
        catch (NoSuchMessageException e) {
            codigo
        }
    }
}
