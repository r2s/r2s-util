package biz.r2s.util

import org.apache.commons.logging.LogFactory

import java.lang.reflect.Method
import java.lang.reflect.Modifier
import java.util.jar.JarEntry
import java.util.jar.JarFile

public class ClassUtils {
    private ClassUtils() {
    }

    private static void checkDirectory(File directory, String pckgname, List<Class<?>> classes) throws ClassNotFoundException {
        File tmpDirectory;

        if (directory.exists() && directory.isDirectory()) {
            final String[] files = directory.list();

            for (final String file : files) {
                if (file.endsWith(".class")) {
                    try {
                        classes.add(Class.forName(pckgname + '.' + file.substring(0, file.length() - 6)));
                    } catch (final NoClassDefFoundError e) {
                        LogFactory.getLog(ClassUtils.class).error(e);
                    }
                } else {
                    tmpDirectory = new File(directory, file);
                    if (tmpDirectory.isDirectory()) {
                        checkDirectory(tmpDirectory, pckgname + "." + file, classes);
                    }
                }
            }
        }
    }

    private static void checkJarFile(JarURLConnection connection, String pckgname, List<Class<?>> classes) throws ClassNotFoundException, IOException {
        final JarFile jarFile = connection.getJarFile();
        final Enumeration<JarEntry> entries = jarFile.entries();
        String name;

        for (JarEntry jarEntry = null; entries.hasMoreElements() && ((jarEntry = entries.nextElement()) != null);) {
            name = jarEntry.getName();

            if (name.contains(".class")) {
                name = name.substring(0, name.length() - 6).replace('/', '.');

                if (name.contains(pckgname)) {
                    classes.add(Class.forName(name));
                }
            }
        }
    }

    public static List<Class<?>> getClassesForPackage(String pckgname) throws ClassNotFoundException {
        final List<Class<?>> classes = new ArrayList<Class<?>>();

        try {
            final ClassLoader cld = Thread.currentThread().getContextClassLoader();

            if (cld == null) {
                lancaExcecaoPersonalizada("Can't get class loader.", new ClassNotFoundException("Can't get class loader."));
            }

            final Enumeration<URL> resources = cld.getResources(pckgname.replace('.', '/'));
            URLConnection connection;

            for (URL url = null; resources.hasMoreElements() && ((url = resources.nextElement()) != null);) {
                connection = url.openConnection();

                if (connection instanceof JarURLConnection) {
                    checkJarFile((JarURLConnection) connection, pckgname, classes);
                } else {
                    checkDirectory(new File(URLDecoder.decode(url.getPath(), "UTF-8")), pckgname, classes);
                }
            }
        } catch (final NullPointerException ex) {
            lancaExcecaoPersonalizada(pckgname + " does not appear to be a valid package (Null pointer exception)", ex);
        } catch (final UnsupportedEncodingException ex) {
            lancaExcecaoPersonalizada(pckgname + " does not appear to be a valid package (Unsupported encoding)", ex);
        } catch (final IOException ioex) {
            lancaExcecaoPersonalizada("IOException was thrown when trying to get all resources for " + pckgname, ioex);
        }

        return classes;
    }

    private static void lancaExcecaoPersonalizada(String mensagem, final Exception ex) throws ClassNotFoundException {
        throw new ClassNotFoundException(mensagem, ex);
    }

    public static Method buscarMetodoPorAnnotation(Class anootationClass, Class classe){
        Method metodoRetorno = null
        Method[] metodos = classe.getMethods()

        for(Method metodo:metodos){
            if(metodo.getAnnotation(anootationClass)){
                metodoRetorno = metodo
            }
        }
        return metodoRetorno
    }

    public static def extrairClosurePorAnnotation(Class anootationClass, Class classe){
        def closure = null

        Method metodo = this.buscarMetodoPorAnnotation(anootationClass, classe)
		if(metodo){
			def obj = classe.newInstance()
            closure = obj.&"${metodo.getName()}"
        }
        return closure
    }

    public static boolean ehSubClasse(Class classe, Class interfacee){
        return !this.ehInterface(classe)&&!this.ehClasseAbstrata(classe)&&interfacee.isAssignableFrom(classe)
    }

    public static boolean ehInterface(Class classe){
        classe.isInterface()
    }

    public static boolean ehClasseAbstrata(Class classe){
        Modifier.isAbstract( classe.getModifiers() )
    }
}
