class R2sUtilBootStrap {

    def customObjectMarshallers
    def init = { servletContext ->
        customObjectMarshallers.register()
    }
    def destroy = {
    }
}
