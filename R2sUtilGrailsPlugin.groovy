import biz.r2s.util.marshaller.CustomObjectMarshallers
import biz.r2s.util.renderer.ApiCollectionRenderer
import biz.r2s.util.renderer.ApiJsonRenderer
import grails.util.Holders

class R2sUtilGrailsPlugin {
    // the plugin version
    def version = "1.0.0"
    // the version or versions of Grails the plugin is designed for
    def grailsVersion = "2.5 > *"
    // resources that are excluded from plugin packaging
    def pluginExcludes = [
        "grails-app/views/error.gsp"
    ]

    def title = "R2s Util Plugin" // Headline display name of the plugin
    def author = "Your name"
    def authorEmail = ""
    def description = '''\
Brief summary/description of the plugin.
'''

    // URL to the plugin's documentation
    def documentation = "http://grails.org/plugin/r2s-util"

    def doWithWebDescriptor = { xml ->
    }

    def doWithSpring = {
        def grailsApplication = Holders.grailsApplication
        customObjectMarshallers(CustomObjectMarshallers)
        grailsApplication.domainClasses.each { domain ->
            "${domain.clazz.simpleName.capitalize()}Renderer"(ApiJsonRenderer, domain.clazz) {}
            "${domain.clazz.simpleName.capitalize()}CollectionRenderer"(ApiCollectionRenderer, domain.clazz) {}
        }
    }

    def doWithDynamicMethods = { ctx ->
    }

    def doWithApplicationContext = { ctx ->
    }

    def onChange = { event ->
    }

    def onConfigChange = { event ->
    }

    def onShutdown = { event ->
    }
}
