package biz.r2s.util.marshaller

import grails.converters.JSON
import grails.util.Holders

/**
 * Created by bruno on 29/04/15.
 */
class CustomObjectMarshallers {

    def grailsApplication = Holders.grailsApplication

    def register() {
        def classes = grailsApplication.domainClasses*.clazz
        registrarTipoCompacto(classes)
        registrarTipoDefault(classes)
        registrarTipoExtended(classes)
    }

    private registrarTipoCompacto(classes) {
        JSON.createNamedConfig('compact') { jason ->
            classes.each { classe ->
                jason.registerObjectMarshaller(classe) { object ->
                    marshalCompact(object)
                }
            }
        }
    }

    private registrarTipoDefault(classes) {
        JSON.createNamedConfig('default') { jason ->
            classes.each { classe ->
                jason.registerObjectMarshaller(classe) { object ->
                    marshalDefault(object)
                }
            }
        }
    }


    private registrarTipoExtended(classes) {
        JSON.createNamedConfig('extended') { jason ->
            classes.each { classe ->
                jason.registerObjectMarshaller(classe) { object ->
                    marshalExtended(object)
                }
            }
        }
    }

    private def tratarProperty(object, nomePropriedade, closureMarshal) {
        if (object."$nomePropriedade" != null) {
            if (grailsApplication.isDomainClass(object."$nomePropriedade".class)) {
                closureMarshal(object."$nomePropriedade")
            } else {
                def obj = object."$nomePropriedade"
                if (obj instanceof Collection) {
                    def list = []
                    obj.each {
                        list << closureMarshal(it)
                    }
                    list
                } else {
					if(obj instanceof Enum){
						Enum e = (Enum) obj
						def o = [
							name: e.name(),
							ordinal: e.ordinal(),
							text: e.toString()
						]
						o
					}
					else
                    	obj
                }
            }
        }
    }

    def marshalCompact = { objectCompact ->
        objectCompact ? [id: objectCompact.id, text: getDisplayText(objectCompact), version: getObjectVersion(objectCompact)] : null
    }

    def marshalDefault = { object ->
        def retorno = marshalCompact(object)
        object?.domainClass?.persistentProperties*.name.each { nomePropriedade ->
            retorno["$nomePropriedade"] = tratarProperty(object, nomePropriedade, marshalCompact)
        }
        retorno
    }

    def marshalExtended = { object ->
        def retorno = marshalCompact(object)
        object?.domainClass?.persistentProperties*.name.each { nomePropriedade ->
            retorno["$nomePropriedade"] = tratarProperty(object, nomePropriedade, marshalDefault)
        }
        retorno
    }

    def marshalCustom = { object, fields ->
        def retorno = null
        if (object != null) {

            retorno = marshalCompact(object)
            fields?.each {
                String campo = ""
                def closure = marshalCompact

                if (it.hasProperty("key")) {
                    campo = it.key
                    if (it.value == TipoMarshal.DEFAULT) {
                        closure = marshalDefault
                    } else if (it.value == TipoMarshal.EXTENDED) {
                        closure = marshalExtended
                    }

                } else {
                    campo = it
                }
                retorno["$campo"] = tratarProperty(object, campo, closure)
            }
        }
        retorno
    }

    private getDisplayText(Object object) {
        return object.toString()
    }

    private def getObjectVersion(obj) {
        def version = 0
        if (obj.hasProperty("version")) {
            version = obj.version
        }
        return version
    }
}
