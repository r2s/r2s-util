package biz.r2s.util.marshaller

/**
 * Created by bruno on 10/06/15.
 */
public enum TipoMarshal {
    COMPACT, DEFAULT, EXTENDED
}
