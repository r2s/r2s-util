package biz.r2s.util

/**
 * Created by raphael on 20/05/15.
 */
interface Hierarquizado {
    Collection getChildrens()
    def getParent()
}