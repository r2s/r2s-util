package biz.r2s.util.renderer

import grails.rest.render.ContainerRenderer
import org.codehaus.groovy.grails.web.mime.MimeType

/**
 * Created by bruno on 30/04/15.
 */
class ApiCollectionRenderer extends ApiJsonRenderer implements ContainerRenderer {
    final Class componentType

    public ApiCollectionRenderer(Class componentType) {
        super(Collection)
        this.componentType = componentType
    }

    public ApiCollectionRenderer(Class componentType, MimeType... mimeTypes) {
        super(Collection, mimeTypes)
        this.componentType = componentType
    }
}
