package biz.r2s.util.renderer

import grails.converters.JSON
import grails.rest.render.AbstractRenderer
import grails.rest.render.RenderContext
import grails.util.GrailsWebUtil
import org.codehaus.groovy.grails.web.mime.MimeType
import org.springframework.http.HttpStatus

/**
 * Created by bruno on 29/04/15.
 */
class ApiJsonRenderer<T> extends AbstractRenderer<T> {

    static int OFFSET_DEFAULT = 0
    static int MAX_DEFAULT = 10

    ApiJsonRenderer(Class<T> targetType) {
        super(targetType, MimeType.JSON)
    }

    @Override
    void render(T object, RenderContext context) {
        context.setContentType(GrailsWebUtil.getContentType(MimeType.JSON.name, GrailsWebUtil.DEFAULT_ENCODING))
        JSON converter

        def out = context.writer
        if (targetType.isAssignableFrom(Collection)) {
            object = trataCollection(object, context.arguments)
        }
        try {
            JSON.use(context.arguments.get("type") ?: "default") {
                converter = object as JSON
            }
        }
        catch (Exception e) {
            context.status = HttpStatus.NOT_ACCEPTABLE
            converter = [mensagem_erro: "Tipo não suportado. Escolha um dos seguintes: compact, default ou extended"]
        }
        converter.render(out)
        out.flush()
        out.close()
    }

    def trataCollection(lista, params) {
        JSON data
        JSON.use(params.get("type") ?: "default") {
            data = lista as JSON
        }
        [
                "recordsFiltered": params.recordsFiltered,
                "recordsTotal"   : params.recordsTotal,
                "draw"           : params.draw,
                "recordsPerPage" : params.max,
                "page"           : params.page,
                "data"           : lista//data.target
        ]

    }
}
